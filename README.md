# executive_smach_tutorials

http://wiki.ros.org/smach/Tutorials

Personal fork of executive_smach_tutorials. Working on some fixes and additions. 
The original repo was cloned from: https://code.ros.org/svn/ros-pkg/stacks/executive_smach_tutorials/trunk by eacousineau.

Clone repo
```bash
git clone https://github.com/ros-visualization/executive_smach_visualization.git
```

If you don't have a problem running
```bash
rosrun smach_viewer smach_viewer.py
```
your env is obsolete, consider upgrade ;)

FIX

add repo containing wxgtk2.8 archive
```bash
sudo nano /etc/apt/sources.list
```

deb http://cz.archive.ubuntu.com/ubuntu trusty main universe

```bash
apt-get update
sudo apt-get install libwxgtk2.8-dev python-wxgtk2.8 python-cairo-dev
```

In file 
```bash
sudo gedit /usr/lib/python2.7/dist-packages/wx-2.8-gtk2-unicode/wx/lib/wxcairo.py
```

change
```python
# This structure is known good with pycairo 1.8.4.
# We have to also test for (1,10,8) because pycairo 1.8.10 has an
# incorrect version_info value
elif cairo.version_info < (1,9) or cairo.version_info == (1,10,8):
    _fields_ = [
        ('Context_Type', ctypes.py_object),
        ('Context_FromContext', ctypes.PYFUNCTYPE(ctypes.py_object,
                                                    ctypes.c_void_p,
                                                    ctypes.py_object,
                                                    ctypes.py_object)),
        ('FontFace_Type', ctypes.py_object),
        ('ToyFontFace_Type', ctypes.py_object),  #** new in 1.8.4
        ('FontFace_FromFontFace', ctypes.PYFUNCTYPE(ctypes.py_object, ctypes.c_void_p)),
        ('FontOptions_Type', ctypes.py_object),
        ('FontOptions_FromFontOptions', ctypes.PYFUNCTYPE(ctypes.py_object, ctypes.c_void_p)),
        ('Matrix_Type', ctypes.py_object),
        ('Matrix_FromMatrix', ctypes.PYFUNCTYPE(ctypes.py_object, ctypes.c_void_p)),
        ('Path_Type', ctypes.py_object),
        ('Path_FromPath', ctypes.PYFUNCTYPE(ctypes.py_object, ctypes.c_void_p)),
        ('Pattern_Type', ctypes.py_object),
        ('SolidPattern_Type', ctypes.py_object),
        ('SurfacePattern_Type', ctypes.py_object),
        ('Gradient_Type', ctypes.py_object),
        ('LinearGradient_Type', ctypes.py_object),
        ('RadialGradient_Type', ctypes.py_object),
        ('Pattern_FromPattern', ctypes.PYFUNCTYPE(ctypes.py_object, ctypes.c_void_p,
                                                    ctypes.py_object)), #** changed in 1.8.4
        ('ScaledFont_Type', ctypes.py_object),
        ('ScaledFont_FromScaledFont', ctypes.PYFUNCTYPE(ctypes.py_object, ctypes.c_void_p)),
        ('Surface_Type', ctypes.py_object),
        ('ImageSurface_Type', ctypes.py_object),
        ('PDFSurface_Type', ctypes.py_object),
        ('PSSurface_Type', ctypes.py_object),
        ('SVGSurface_Type', ctypes.py_object),
        ('Win32Surface_Type', ctypes.py_object),
        ('XlibSurface_Type', ctypes.py_object),
        ('Surface_FromSurface', ctypes.PYFUNCTYPE(ctypes.py_object,
                                                    ctypes.c_void_p,
                                                    ctypes.py_object)),
        ('Check_Status', ctypes.PYFUNCTYPE(ctypes.c_int, ctypes.c_int))]
```

to

```python
# This structure is known good with pycairo 1.11.1+.
else:
    _fields_ = [
        ('Context_Type', ctypes.py_object),
        ('Context_FromContext', ctypes.PYFUNCTYPE(ctypes.py_object,
                                                    ctypes.c_void_p,
                                                    ctypes.py_object,
                                                    ctypes.py_object)),
        ('FontFace_Type', ctypes.py_object),
        ('ToyFontFace_Type', ctypes.py_object),
        ('FontFace_FromFontFace', ctypes.PYFUNCTYPE(ctypes.py_object, ctypes.c_void_p)),
        ('FontOptions_Type', ctypes.py_object),
        ('FontOptions_FromFontOptions', ctypes.PYFUNCTYPE(ctypes.py_object, ctypes.c_void_p)),
        ('Matrix_Type', ctypes.py_object),
        ('Matrix_FromMatrix', ctypes.PYFUNCTYPE(ctypes.py_object, ctypes.c_void_p)),
        ('Path_Type', ctypes.py_object),
        ('Path_FromPath', ctypes.PYFUNCTYPE(ctypes.py_object, ctypes.c_void_p)),
        ('Pattern_Type', ctypes.py_object),
        ('SolidPattern_Type', ctypes.py_object),
        ('SurfacePattern_Type', ctypes.py_object),
        ('Gradient_Type', ctypes.py_object),
        ('LinearGradient_Type', ctypes.py_object),
        ('RadialGradient_Type', ctypes.py_object),
        ('Pattern_FromPattern', ctypes.PYFUNCTYPE(ctypes.py_object, ctypes.c_void_p,
                                                    ctypes.py_object)), #** changed in 1.8.4
        ('ScaledFont_Type', ctypes.py_object),
        ('ScaledFont_FromScaledFont', ctypes.PYFUNCTYPE(ctypes.py_object, ctypes.c_void_p)),
        ('Surface_Type', ctypes.py_object),
        ('ImageSurface_Type', ctypes.py_object),
        ('PDFSurface_Type', ctypes.py_object),
        ('PSSurface_Type', ctypes.py_object),
        ('SVGSurface_Type', ctypes.py_object),
        ('Win32Surface_Type', ctypes.py_object),
        ('Win32PrintingSurface_Type', ctypes.py_object),  #** new
        ('XCBSurface_Type', ctypes.py_object),            #** new
        ('XlibSurface_Type', ctypes.py_object),
        ('Surface_FromSurface', ctypes.PYFUNCTYPE(ctypes.py_object,
                                                    ctypes.c_void_p,
                                                    ctypes.py_object)),
        ('Check_Status', ctypes.PYFUNCTYPE(ctypes.c_int, ctypes.c_int)),
        ('RectangleInt_Type', ctypes.py_object),
        ('RectangleInt_FromRectangleInt', ctypes.PYFUNCTYPE(ctypes.py_object, ctypes.c_void_p)),
        ('Region_Type', ctypes.py_object),
        ('Region_FromRegion', ctypes.PYFUNCTYPE(ctypes.py_object, ctypes.c_void_p)),
        ('RecordingSurface_Type', ctypes.py_object)]
```

THIS IS PYTHON !!! TABS ARE CRITICAL